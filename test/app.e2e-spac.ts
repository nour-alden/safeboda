import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import initTestAppWithToken from './initTestAppWithToken';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const { testApp } = await initTestAppWithToken(false);
    app = testApp;
  });

  it('Health', () => {
    return request(app.getHttpServer()).get('/health').expect(200).expect({
      health: true,
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
