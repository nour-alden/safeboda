import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';

import { AppModule } from '../src/app.module';
import * as request from 'supertest';
import * as faker from 'faker';
import { RideStatusEnum } from '../src/ride/enums';
import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();

const createFakeDriver = async (length = 100) => {
  for (let i = 0; i < length; i++) {
    const data = {
      suspended: faker.datatype.boolean(),
      name: faker.name.findName(),
      phoneNumber: faker.phone.phoneNumber(),
    };
    await prisma.driver.create({ data });
  }
};

const createFakePassenger = async (length = 100) => {
  for (let i = 0; i < length; i++) {
    const data = {
      name: faker.name.findName(),
      phoneNumber: faker.phone.phoneNumber(),
    };
    await prisma.passenger.create({ data });
  }
};

const createFakeRide = async (length = 20) => {
  for (let i = 0; i < length; i++) {
    const driver = await prisma.driver.findFirst({
      skip: i,
      where: { suspended: false },
    });
    const passenger = await prisma.passenger.findFirst({ skip: i });
    try {
      await prisma.ride.create({
        data: {
          latPickup: 0.0,
          longPickup: 0.0,
          latDestination: 0.0,
          longDestination: 0.0,
          driver: { connect: { id: driver.id } },
          passenger: { connect: { id: passenger.id } },
          status: faker.datatype.boolean()
            ? RideStatusEnum.STOP
            : RideStatusEnum.ONGOING,
        },
      });
    } catch (e) {}
  }
};

export default async (
  withFakeDate = true,
): Promise<{
  testToken: string;
  testApp: INestApplication;
}> => {
  let testToken = '';

  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();

  const testApp = moduleFixture.createNestApplication();
  testApp.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
      whitelist: true,
    }),
  );
  await testApp.init();

  await request(testApp.getHttpServer())
    .post('/login')
    .send({
      email: process.env.INIT_ADMIN_EMAIL,
      password: process.env.INIT_ADMIN_PASSWORD,
    })
    .expect(async (response) => {
      testToken = response.text;
    });

  if (withFakeDate) {
    await createFakeDriver();
    await createFakePassenger();
    await createFakeRide();
  }

  return { testApp, testToken };
};
