import { INestApplication } from '@nestjs/common';
import initTestAppWithToken from './initTestAppWithToken';
import * as request from 'supertest';
import { PassengerErrors } from '../src/passenger/passenger.error';

describe('PassengerController (e2e)', () => {
  let app: INestApplication;

  let loggedInToken = '';

  beforeAll(async () => {
    const { testApp, testToken } = await initTestAppWithToken(false);
    app = testApp;
    loggedInToken = testToken;
  });

  it('create passenger without token', () => {
    return request(app.getHttpServer())
      .post('/passenger')
      .send({
        name: 'test',
        phoneNumber: '+13253243253',
      })
      .expect(401)
      .expect({
        statusCode: 401,
        message: 'Unauthorized',
      });
  });
  it('create passenger', () => {
    return request(app.getHttpServer())
      .post('/passenger')
      .set('Authorization', `bearer ${loggedInToken}`)
      .send({
        name: 'test',
        phoneNumber: '+13253243253',
      })
      .expect(async (res) => {
        await expect(res.body.name).toBe('test');
        await expect(res.body.phoneNumber).toBe('+13253243253');
      });
  });
  it('create passenger already Added', () => {
    return request(app.getHttpServer())
      .post('/passenger')
      .set('Authorization', `bearer ${loggedInToken}`)
      .send({
        name: 'test',
        phoneNumber: '+13253243253',
      })
      .expect(400)
      .expect(PassengerErrors.alreadyAdded);
  });

  afterAll(async () => {
    await app.close();
  });
});
