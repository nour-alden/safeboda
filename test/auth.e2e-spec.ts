import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import initTestAppWithToken from './initTestAppWithToken';

describe('AuthController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const { testApp } = await initTestAppWithToken(false);
    app = testApp;
  });

  it('login success', () => {
    return request(app.getHttpServer())
      .post('/login')
      .send({
        email: process.env.INIT_ADMIN_EMAIL,
        password: process.env.INIT_ADMIN_PASSWORD,
      })
      .expect(201)
      .expect((res) => {
        expect(res.text).toBeDefined();
      });
  });
  it('login wrong password', () => {
    return request(app.getHttpServer())
      .post('/login')
      .send({
        email: process.env.INIT_ADMIN_EMAIL,
        password: process.env.INIT_ADMIN_PASSWORD + 'wrong',
      })
      .expect(400)
      .expect({
        message: 'auth/incorrect-email-or-password',
        code: 101,
      });
  });

  afterAll(async () => {
    await app.close();
  });
});
