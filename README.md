# SafeBoda Test

## Technologies used

### [NestJS](https://nestjs.com/)
>A progressive Node.js framework for building efficient and scalable server-side applications, heavily inspired by Angular.


### [prismaJs](https://www.prisma.io/)
> Typescript ORM



## Installation

```bash
 yarn 
```

##add .env file
```
DATABASE_URL="file:./dev.db" 
INIT_ADMIN_EMAIL=admin@admin.com
INIT_ADMIN_PASSWORD=safebode
ENVIRONMENT=test
JWT_SECRET_KEY=secret_kufh34ngmc8kdymndvyx
```
##add .env.test file for testing
```
DATABASE_URL="file:./test.db" 
INIT_ADMIN_EMAIL=admin@admin.com
INIT_ADMIN_PASSWORD=safebode
ENVIRONMENT=test
JWT_SECRET_KEY=secret_kufh34ngmc8kdymndvyx
```

## DB migrate
```bash
 yarn prisma:migrate 
 yarn prisma:generate
```

##DB Schema file 
> /prisma/schema.prisma

## Running the app


```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

> after running app you can find swagger documentation on 
> 
> http://localhost:3000/api/

### show project modules  docs
```bash
$ yarn docs
```

###You can open prisma studio and view the data and manipulate it

```bash
$ yarn prisma:studio
```

## Test


```bash
# e2e tests
$ yarn test:e2e
```











