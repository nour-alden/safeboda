import {
  Controller,
  Body,
  Post,
  Param,
  HttpCode,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { DriverService } from './driver.service';
import { CreateDriverDto } from './dto';
import { ApiResponse, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Driver')
@ApiBearerAuth()
@UseGuards(AuthGuard())
@Controller('driver')
export class DriverController {
  constructor(private readonly driverService: DriverService) {}

  @ApiResponse({ status: 201, description: 'return driver data' })
  @ApiResponse({ status: 400, description: 'driver/already-added' })
  @Post()
  create(@Body() createDriverDto: CreateDriverDto) {
    return this.driverService.createDriver(createDriverDto);
  }

  @ApiResponse({ status: 204, description: 'Done' })
  @ApiResponse({ status: 404, description: 'driver/not-found' })
  @ApiResponse({ status: 400, description: 'driver/already-suspended' })
  @ApiResponse({
    status: 400,
    description: 'driver/cannot-suspended-has-ongoing-ride',
  })
  @Post(':id/suspend')
  @HttpCode(204)
  suspendDriver(@Param('id') id: number) {
    return this.driverService.suspendDriver(id);
  }

  @ApiResponse({ status: 204, description: 'Done' })
  @ApiResponse({ status: 404, description: 'driver/not-found' })
  @ApiResponse({ status: 400, description: 'driver/not-suspended' })
  @Delete(':id/suspend')
  @HttpCode(204)
  remove(@Param('id') id: number) {
    return this.driverService.deleteSuspendDriver(id);
  }
}
