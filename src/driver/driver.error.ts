export const DriverErrors = {
  alreadyAdded: {
    message: 'driver/already-added',
    code: 200,
  },
  alreadySuspended: {
    message: 'driver/already-suspended',
    code: 201,
  },
  notFound: {
    message: 'driver/not-found',
    code: 202,
  },
  notSuspended: {
    message: 'driver/not-suspended',
    code: 203,
  },
  suspended: {
    message: 'driver/suspended',
    code: 204,
  },
  cannotSuspended: {
    message: 'driver/cannot-suspended-has-ongoing-ride',
    code: 205,
  },
};
