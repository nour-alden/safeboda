import { IsString, IsMobilePhone } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class CreateDriverDto {
  @ApiProperty()
  @IsString()
  name: string;
  @ApiProperty()
  @IsMobilePhone()
  phoneNumber: string;
}
