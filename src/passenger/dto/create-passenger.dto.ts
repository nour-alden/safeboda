import { ApiProperty } from '@nestjs/swagger';
import { IsMobilePhone, IsString } from 'class-validator';

export class CreatePassengerDto {
  @ApiProperty()
  @IsString()
  name: string;
  @ApiProperty()
  @IsMobilePhone()
  phoneNumber: string;
}
