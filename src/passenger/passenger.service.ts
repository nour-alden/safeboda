import { BadRequestException, Injectable } from '@nestjs/common';
import { Prisma, Passenger } from '@prisma/client';
import { PrismaService } from '../prisma.service';
import { PassengerErrors } from './passenger.error';

@Injectable()
export class PassengerService {
  constructor(private readonly prisma: PrismaService) {}

  async passenger(
    passengerWhereUniqueInput: Prisma.PassengerWhereUniqueInput,
  ): Promise<Passenger | null> {
    return this.prisma.passenger.findUnique({
      where: passengerWhereUniqueInput,
    });
  }

  // Not used yet
  async passengers(params: {
    skip?: number;
    take?: number;
    where?: Prisma.PassengerWhereInput;
    orderBy?: Prisma.PassengerOrderByWithRelationInput;
  }): Promise<Passenger[]> {
    const { skip, take, where, orderBy } = params;
    return this.prisma.passenger.findMany({
      skip,
      take,
      where,
      orderBy,
    });
  }

  async createPassenger(data: Prisma.PassengerCreateInput): Promise<Passenger> {
    const passenger = await this.passenger({ phoneNumber: data.phoneNumber });
    if (passenger) {
      throw new BadRequestException(PassengerErrors.alreadyAdded);
    }
    return this.prisma.passenger.create({
      data,
    });
  }

  // Not used yet
  async updatePassenger(params: {
    where: Prisma.PassengerWhereUniqueInput;
    data: Prisma.PassengerUpdateInput;
  }): Promise<Passenger> {
    const { where, data } = params;
    return this.prisma.passenger.update({
      data,
      where,
    });
  }

  // Not used yet
  async deletePassenger(
    where: Prisma.PassengerWhereUniqueInput,
  ): Promise<Passenger> {
    return this.prisma.passenger.delete({
      where,
    });
  }
}
