export enum RideStatusEnum {
  STOP = 'stop',
  ONGOING = 'ongoing',
}
