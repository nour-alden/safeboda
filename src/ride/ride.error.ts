export const RideError = {
  driverNotAvailable: {
    message: 'ride/driver-not-available-now',
    code: 400,
  },
  passengerNotAvailable: {
    message: 'ride/passenger-not-available-now',
    code: 401,
  },
  notFound: {
    message: 'ride/not-found',
    code: 402,
  },
  alreadyStop: {
    message: 'ride/already-stop',
    code: 403,
  },
};
