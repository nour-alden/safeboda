import { Controller, Post, Body, Param, Get, UseGuards } from '@nestjs/common';
import { RideService } from './ride.service';
import { RideLocationDto } from './dto';
import { RideStatusEnum } from './enums';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Ride')
@ApiBearerAuth()
@UseGuards(AuthGuard())
@Controller('ride')
export class RideController {
  constructor(private readonly rideService: RideService) {}

  @ApiResponse({ status: 201, description: 'return ride data' })
  @ApiResponse({ status: 404, description: 'ride/not-found' })
  @ApiResponse({ status: 400, description: 'ride/already-stop' })
  @Post(':id/stop')
  stopRide(@Param('id') id: number) {
    return this.rideService.stopRide(id);
  }

  @ApiBearerAuth()
  @ApiResponse({ status: 201, description: 'return ride data' })
  @ApiResponse({ status: 404, description: 'driver/not-found' })
  @ApiResponse({ status: 400, description: 'driver/already-suspended' })
  @ApiResponse({ status: 404, description: 'passenger/not-found' })
  @ApiResponse({ status: 400, description: 'ride/passenger-not-available-no' })
  @ApiResponse({ status: 400, description: 'ride/driver-not-available-now' })
  @Post(':passengerId/:driverId')
  async create(
    @Param('passengerId') passengerId: number,
    @Param('driverId') driverId: number,
    @Body() rideLocationDto: RideLocationDto,
  ) {
    return this.rideService.createRideForDriverAndPassenger(
      driverId,
      passengerId,
      rideLocationDto,
    );
  }

  @ApiResponse({ status: 200, description: 'return array of rides' })
  @Get('/ongoing')
  async getOngoingRide() {
    return this.rideService.rides({
      where: { status: RideStatusEnum.ONGOING },
    });
  }
}
