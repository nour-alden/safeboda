import {
  Injectable,
  Logger,
  OnModuleInit,
  BadRequestException,
} from '@nestjs/common';
import { genSalt, hash, compare } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

import { Prisma, User } from '@prisma/client';
import { AuthErrors } from './auth.error';
import { JwtPayloadDto, LoginDto } from './dtos';
import { PrismaService } from '../prisma.service';

@Injectable()
export class AuthService implements OnModuleInit {
  private readonly logger = new Logger(AuthService.name);
  constructor(
    private readonly jwtService: JwtService,
    private readonly prisma: PrismaService,
  ) {}

  async onModuleInit(): Promise<void> {
    await this.addInitAdmin();
  }

  async hashPassword(password: string): Promise<string> {
    const salt = await genSalt(6);
    return hash(password, salt);
  }

  async addInitAdmin(): Promise<void> {
    try {
      if (process.env.ENVIRONMENT !== 'test') return;
      if (!process.env.INIT_ADMIN_EMAIL || !process.env.INIT_ADMIN_PASSWORD) {
        this.logger.error(
          'please add INIT_ADMIN_EMAIL && INIT_ADMIN_PASSWORD To environment variables',
        );
      }

      await this.register({
        email: process.env.INIT_ADMIN_EMAIL,
        password: process.env.INIT_ADMIN_PASSWORD,
      });
      this.logger.verbose(
        `create init admin email:(${process.env.INIT_ADMIN_EMAIL}) password:(${process.env.INIT_ADMIN_PASSWORD})`,
      );
    } catch (e) {
      //admin added before
    }
  }

  // It is currently used only to add the default admin, and it can be used later for the process of creating an admin account
  async register(
    newUser: Prisma.UserCreateInput,
  ): Promise<User | BadRequestException> {
    const { email, password } = newUser;
    const user = await this.prisma.user.findUnique({ where: { email } });
    if (user) {
      throw new BadRequestException(AuthErrors.alreadyUse);
    }
    const hashedPassword = await this.hashPassword(password);
    return this.prisma.user.create({
      data: {
        email,
        password: hashedPassword,
      },
    });
  }

  async generateToken(payload: JwtPayloadDto): Promise<string> {
    return this.jwtService.sign(payload);
  }

  async login(loginDto: LoginDto) {
    const { email, password } = loginDto;
    const user = await this.prisma.user.findUnique({ where: { email } });

    if (!user) {
      throw new BadRequestException(AuthErrors.loginError);
    }
    const isPasswordValid = await compare(password, user.password);
    if (!isPasswordValid) {
      throw new BadRequestException(AuthErrors.loginError);
    }
    return this.generateToken({ id: user.id });
  }
}
