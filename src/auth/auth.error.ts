export const AuthErrors = {
  alreadyUse: {
    message: 'auth/email-already-in-use',
    code: 100,
  },
  loginError: {
    message: 'auth/incorrect-email-or-password',
    code: 101,
  },
};
